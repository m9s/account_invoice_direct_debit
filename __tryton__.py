#This file is part of Tryton. The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
{
    'name': 'Account Invoice Direct Debit',
    'name_de_DE': 'Fakturierung Zahlungsziel "Einzugsermächtigung"',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz/',
    'description': '''Account Invoice Direct Debit
    - Provides the payment_term "direct debit", actually
      connected to the standard term for partial payment.
''',
    'description_de_DE': '''Zahlungsziel "Einzugsermächtigung" für Rechnungen
    - Stellt die "Einzugsermächtigung" als Zahlungsziel zur Verfügung.
''',
    'depends': [
        'account_invoice_cash_discount',
    ],
    'xml': [
        'payment_term.xml'
    ],
    'translation': [
        'locale/de_DE.po',
    ],
}
