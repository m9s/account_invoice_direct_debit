#This file is part of Tryton. The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
import copy
from trytond.model import ModelView, ModelSQL, fields
from trytond.pyson import Not, Equal, Eval, And, Bool


class PaymentTerm(ModelSQL, ModelView):
    _name = 'account.invoice.payment_term'

    def __init__(self):
        super(PaymentTerm, self).__init__()
        self.lines = copy.copy(self.lines)
        if not self.lines.states:
            self.lines.states={'invisible': Not(Bool(Equal(Eval('type'),
                    'direct_debit')))}
        else:
            if 'invisible' not in self.lines.states:
                self.lines.states.update({'invisible': Not(Bool(Equal(Eval('type'),
                        'direct_debit')))})
            else:
                self.lines.states['invisible'] = And(
                        self.lines.states['invisible'],
                        Not(Bool(Equal(Eval('type'), 'direct_debit'))))
        if 'type' not in self.lines.depends:
            self.lines.depends = copy.copy(self.lines.depends)
            self.lines.depends.append('type')
        self._reset_columns()

PaymentTerm()
